import 'package:first_app/screens/login/login_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: LogInScreen(),
    theme: ThemeData(
      primaryColor: Colors.blue[900],
      accentColor: Colors.grey.shade800,
      brightness: Brightness.light,
      fontFamily: 'Satisfy',
      cardColor: Colors.amber[50],
    ),
  ));
}
