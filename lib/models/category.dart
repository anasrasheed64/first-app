class Category {
  String name;
  String imagePath;

  Category({ this.name,  this.imagePath});
}

// List<Category> ?loadCategory() {
//   var category = <Category>[
//     Category(name: "fruit", imagePath: 'assets/fruit.png'),
//     Category(name: "vegetabels", imagePath: 'assets/vegetabel.png'),
//     Category(name: 'bakery', imagePath: 'assets/bread.png'),
//     Category(name: 'dairy', imagePath: 'assets/dairy.png'),
//   ];
//}
