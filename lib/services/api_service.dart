import 'dart:collection';
import 'dart:io';

import 'package:first_app/utils/ui/popup_helper.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiService {
  Future<http.Response> _getRequest({
    BuildContext context,
    String url,
  }) async {
    http.Response response;
    try {
      response = await http.get(
        Uri.parse(url),
        headers: await _getHeaders(context),
      );
      debugPrint(url + response.body);
    } on SocketException {
      PobUpHelper.instance.showToast(
          context: context, message: "Please check your internet connection");
    } catch (e) {
      throw e;
    }
    return response;
  }

  Future<http.Response> postRequest({
    @required BuildContext context,
    @required String url,
    Map<String, String> body,
  }) async {
    http.Response response;
    try {
      response = await http.post(
        Uri.parse(url),
        body: body,
        headers: await _getHeaders(context),
      );
      debugPrint(url + response.body);
    } on SocketException {
      PobUpHelper.instance.showToast(
          context: context, message: "Please check your internet connection");
    } catch (e) {
      throw e;
    }
    return response;
  }

  Future<http.Response> _putRequest({
    BuildContext context,
    String url,
    Map<String, String> body,
  }) async {
    http.Response response;
    try {
      response = await http.put(
        Uri.parse(url),
        body: body,
        headers: await _getHeaders(context),
      );
      debugPrint(url + response.body);
    } on SocketException {
      PobUpHelper.instance.showToast(
          context: context, message: "Please check your internet connection");
    } catch (e) {
      throw e;
    }
    return response;
  }

  Future<Map<String, String>> _getHeaders(BuildContext context) async {
    HashMap<String, String> headers = HashMap();
    headers[HttpHeaders.acceptHeader] = 'application/json';
    headers['Language'] =
        Localizations.localeOf(context).languageCode.toLowerCase();
    return headers;
  }
}
