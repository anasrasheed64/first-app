import 'dart:convert';

RegistrationResponse registrationResponseFromJson(String str) => RegistrationResponse.fromJson(json.decode(str));

String registrationResponseToJson(RegistrationResponse data) => json.encode(data.toJson());

class RegistrationResponse {
  RegistrationResponse({
    this.status,
    this.data,
  });

  final int status;
  final Data data;

  factory RegistrationResponse.fromJson(Map<String, dynamic> json) => RegistrationResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.jsonObject,
  });

  final JsonObject jsonObject;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    jsonObject: json["json_object"] == null ? null : JsonObject.fromJson(json["json_object"]),
  );

  Map<String, dynamic> toJson() => {
    "json_object": jsonObject == null ? null : jsonObject.toJson(),
  };
}

class JsonObject {
  JsonObject({
    this.token,
    this.user,
  });

  final String token;
  final User user;

  factory JsonObject.fromJson(Map<String, dynamic> json) => JsonObject(
    token: json["token"] == null ? null : json["token"],
    user: json["user"] == null ? null : User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "token": token == null ? null : token,
    "user": user == null ? null : user.toJson(),
  };
}

class User {
  User({
    this.name,
    this.username,
    this.email,
    this.phone,
    this.emailVerifiedAt,
    this.phoneVerifiedAt,
    this.deviceToken,
    this.status,
    this.updatedAt,
    this.createdAt,
    this.id,
  });

  final String name;
  final String username;
  final String email;
  final String phone;
  final dynamic emailVerifiedAt;
  final dynamic phoneVerifiedAt;
  final String deviceToken;
  final bool status;
  final DateTime updatedAt;
  final DateTime createdAt;
  final int id;

  factory User.fromJson(Map<String, dynamic> json) => User(
    name: json["name"] == null ? null : json["name"],
    username: json["username"] == null ? null : json["username"],
    email: json["email"] == null ? null : json["email"],
    phone: json["phone"] == null ? null : json["phone"],
    emailVerifiedAt: json["email_verified_at"],
    phoneVerifiedAt: json["phone_verified_at"],
    deviceToken: json["device_token"] == null ? null : json["device_token"],
    status: json["status"] == null ? null : json["status"],
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    id: json["id"] == null ? null : json["id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "username": username == null ? null : username,
    "email": email == null ? null : email,
    "phone": phone == null ? null : phone,
    "email_verified_at": emailVerifiedAt,
    "phone_verified_at": phoneVerifiedAt,
    "device_token": deviceToken == null ? null : deviceToken,
    "status": status == null ? null : status,
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "id": id == null ? null : id,
  };
}