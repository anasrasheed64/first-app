import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefranceHelper{
  Future<String>read({String key=''})async{
    final prefs=await SharedPreferences.getInstance();
    final value=prefs.getString(key)??'';
    if(value==null){
      return "";
    }
    return value;
  }
  Future<void>save({String key='',String value=''})async{
    if(key==null||value==null){
      return;
    }
    final prefs=await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }
  Future<void>deleteAll()async{
    final prefs=await SharedPreferences.getInstance();
    prefs.clear();
  }
}