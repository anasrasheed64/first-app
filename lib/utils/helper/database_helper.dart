import 'dart:async';
import 'dart:io';
import 'package:first_app/models/product.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  //static const NEW_DB_VERSION = 6;
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;
 Database _db;
  final String tableProduct = "Product";
  final String columnImagePath = "imagePath";
  final String columnProductName = "productName";
  final String columnQuantity = 'quantity';
  final String columnDescription = "description";
  final String columnPrice = 'price';

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "main.db");
    var ourDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE $tableProduct(id INTEGER PRIMARY KEY AUTOINCREMENT,$columnProductName TEXT,$columnImagePath TEXT,$columnDescription Text,$columnPrice Text,$columnQuantity Text)");
    print("Table is Created");
  }

  Future<int> saveProduct(Product product) async {
    var dbClient = await db;
    int res = await dbClient.insert(tableProduct, product.toMap());
    print(res);
    return res;
  }
  Future<List<Product>>getAllProducts()async{
    List<Product>allProducts=[];
    var dbClient=await db;
    List<Map>maps=await dbClient.query(tableProduct,
      columns:[
      columnImagePath,
      columnProductName,
      columnDescription,
      columnPrice.toString(),
      columnQuantity.toString(),

      ],);
    print(maps);
    if(maps!=null &&maps.length>0){
      for(int index=0;index<maps.length;index++){
        allProducts.add(Product.map(index, maps));
      }
      return allProducts;
    }else{
    return null;}
  }

}