// import 'package:flutter_sound_lite/public/flutter_sound_recorder.dart';
// import 'package:permission_handler/permission_handler.dart';
// import '';
//
// class SoundRecorder {
//   FlutterSoundRecorder _audioRecorder;
//   bool _isRecorderInitialized =false;
// bool get isRecording =>_audioRecorder.isRecording;
//   Future init() async {
//     _audioRecorder = FlutterSoundRecorder();
//     final status = await Permission.microphone.request();
//     if (status != PermissionStatus.granted) {
//       throw RecordingPermissionException("mic permission error");
//     }
//     await _audioRecorder.openAudioSession();
//     _isRecorderInitialized = true;
//     //open audiosession
//   }
//
//   void dispose() {
//     if (!_isRecorderInitialized) return;
//
//     //close audiosession
//     _audioRecorder.closeAudioSession();
//     _audioRecorder = null;
//     _isRecorderInitialized = false;
//   }
//
//   Future _record() async {
//     if (!_isRecorderInitialized) return;
//
//     await _audioRecorder.startRecorder();
//   }
//
//   Future _stop() async {
//     if (!_isRecorderInitialized) return;
//
//     await _audioRecorder.stopRecorder();
//   }
//
//   Future toggleRecording() async {
//     if (_audioRecorder.isStopped) {
//       await _record();
//     } else {
//       await _stop();
//     }
//   }
// }
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:path_provider/path_provider.dart';

class SoundRecorder extends StatefulWidget {
  final Function onSaved;



  const SoundRecorder({Key key,this.onSaved}) : super(key: key);

  @override
  _SoundRecorderState createState() => _SoundRecorderState();
}
// enum RecordingState {
//   UnSet,
//   Set,
//   Recording,
//   Stopped,
// }
class _SoundRecorderState extends State<SoundRecorder> {
  IconData _recordIcon = Icons.mic_none;
  String _recordText = 'Click To Start';
  RecordingStatus _recordingState = RecordingStatus.Unset;
FlutterAudioRecorder _audioRecorder;
  @override
  void init() {
    super.initState();

    FlutterAudioRecorder.hasPermissions.then((hasPermision) {
      if (hasPermision) {
        _recordingState = RecordingStatus.Initialized;
        _recordIcon = Icons.mic;
        _recordText = 'Record';
      }
    });
  }
  @override
  void dispose() {
    _recordingState = RecordingStatus.Unset;
    _audioRecorder=null;
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.center,
      children: [
        MaterialButton(
          onPressed: () async {
            await _onRecordButtonPressed();
            setState(() {});
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          child: Container(
            width: 150,
            height: 150,
            child: Icon(
              _recordIcon,
              size: 50,
            ),
          ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              child: Text(_recordText),
              padding: const EdgeInsets.all(8),
            ))
      ],);
  }
  Future<void> _onRecordButtonPressed() async {
    switch (_recordingState) {
      case RecordingStatus.Initialized:
        await _recordVoice();
        break;

      case RecordingStatus.Recording:
        await _stopRecording();
        _recordingState = RecordingStatus.Stopped;
        _recordIcon = Icons.fiber_manual_record;
        _recordText = 'Record new one';
        break;

      case RecordingStatus.Stopped:
        await _recordVoice();
        break;

      case RecordingStatus.Unset:
        ScaffoldMessenger.of(context).hideCurrentSnackBar();

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Please allow recording from settings.'),
        ));
        break;
    }
  }
  _initRecorder() async {
    Directory appDirectory = await getApplicationDocumentsDirectory();
    String filePath = appDirectory.path +
        '/' +
        DateTime.now().millisecondsSinceEpoch.toString() +
        '.aac';

    _audioRecorder =
        FlutterAudioRecorder(filePath, audioFormat: AudioFormat.AAC);
    await _audioRecorder.initialized;
  }

  _startRecording() async {
    await _audioRecorder.start();
    // await audioRecorder.current(channel: 0);
  }

  _stopRecording() async {
    await _audioRecorder.stop();
setState(() {
  _recordIcon=Icons.mic;
});
   // widget.onSaved();
  }

  Future<void> _recordVoice() async {
    final hasPermission = await FlutterAudioRecorder.hasPermissions;
    if (hasPermission ?? false) {
      await _initRecorder();

      await _startRecording();
      _recordingState = RecordingStatus.Recording;
      _recordIcon = Icons.stop;
      _recordText = 'Recording';
    } else {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Please allow recording from settings.'),
      ));
    }
  }
}