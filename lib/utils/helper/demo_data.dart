import 'package:first_app/models/category.dart';
import 'package:first_app/models/product.dart';

class DemoData {
  static List<Category> categories = [
    Category(name: "fruit", imagePath: 'assets/fruit.png'),
    Category(name: "vegetabels", imagePath: 'assets/vegetabel.png'),
    Category(name: 'bakery', imagePath: 'assets/bread.png'),
    Category(name: 'dairy', imagePath: 'assets/dairy.png'),
  ];
  static List<Product> products = [
    Product(
        name: "Strowberry",
        price: 2.5,
        img: "assets/strowberry.png",
        desc:
            "A strawberry is both a low-growing, flowering plant and also the name of the fruit that it produces. Strawberries are soft, sweet, bright red berries.",
        quantity: 0),
    Product(
        name: "Bnana",
        price: 2.5,
        img: "assets/bnana.png",
        desc:
            "A banana is a curved, yellow fruit with a thick skin and soft sweet flesh,A banana is a tropical fruit that's quite popular all over the world. It grows in bunches on a banana tree.",
        quantity: 0),
    Product(
        name: "Kiwi",
        price: 2.5,
        img: "assets/kiwi.png",
        desc:
            "The ellipsoidal kiwi fruit is a true berry and has furry brownish green skin. The firm translucent green flesh has numerous edible purple-black seeds embedded around a white centre.",
        quantity: 0),
    Product(
        name: "Watermelon",
        price: 2.5,
        img: "assets/watermelon.png",
        desc:
            "The watermelon is a large fruit of a more or less spherical shape. ... It has an oval or spherical shape and a dark green and smooth rind, sometimes showing irregular areas of a pale green colour.",
        quantity: 0),
  ];
}
