import 'package:first_app/models/category.dart';
import 'package:flutter/material.dart';

class CategoryItemSearch extends SearchDelegate<Category> {

  final List<Category> categories;

  CategoryItemSearch(this.categories);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: Icon(Icons.clear_outlined))
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {

    return IconButton(
        onPressed: () {
          //close(context,Nav);
          close(context, Category(name: "", imagePath: ""));
        },
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ));
  }

  @override
  Widget buildResults(BuildContext context) {
    List<Category> result =
    categories.where((element) => element.name.contains(query)).toList();
    if (query.isEmpty) result = categories;
    return GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: result.length,
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          //childAspectRatio: 3/2,
          crossAxisSpacing: 15,
          mainAxisSpacing: 20,
        ),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            // width: 2,
            // height: 1,
            padding: EdgeInsets.all(12),
            child: Card(
              color: Theme.of(context).cardColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    result[index].imagePath.toString(),
                    width: 110,
                    height: 100,
                  ),
                  Text(
                    result[index].name.toString(),
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {

    List<Category> result =
    categories.where((element) => element.name.contains(query)).toList();
    if (query.isEmpty) result = categories;
    return GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: result.length,
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          //childAspectRatio: 3/2,
          crossAxisSpacing: 15,
          mainAxisSpacing: 20,
        ),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            // width: 2,
            // height: 1,
            padding: EdgeInsets.all(12),
            child: Card(
              color: Theme.of(context).cardColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    result[index].imagePath.toString(),
                    width: 110,
                    height: 100,
                  ),
                  Text(
                    result[index].name.toString(),
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ],
              ),
            ),
          );
        });
  }
}