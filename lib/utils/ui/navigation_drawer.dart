import 'package:cached_network_image/cached_network_image.dart';
import 'package:first_app/screens/audio_screen/audio_screen.dart';
import 'package:first_app/screens/category_screen/category_screen.dart';
import 'package:first_app/screens/extra/extra_screen.dart';
import 'package:first_app/screens/home/home_screen.dart';
import 'package:first_app/screens/map_screen/map_screen.dart';
import 'package:flutter/material.dart';

class NavigationDrawer extends StatelessWidget {
  //const NavigationDrawer({Key? key}) : super(key: key);
  final padding = EdgeInsets.symmetric(horizontal: 20);

  @override
  Widget build(BuildContext context) {
    String userName = "User Name";
    String mobileNumber = "Mobile Number";
    //String img =
    //  ("https://www.youloveit.com/uploads/posts/2020-11/1605537237_youloveit_com_disney_princess_wears_masks_profile_pictures09.jpeg");

    return Drawer(
      child: Material(
        color: Colors.white,
        child: Container(
          child: ListView(
            // padding: padding,
            children: [
              InkWell(
                //child: Container(padding: EdgeInsets.symmetric(horizontal: 20,vertical: 40),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HomeScreen(
                                index: 1,
                              )));
                },
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      child: CachedNetworkImage(
                        imageUrl: "https://www.geeklawblog.com/wp-content/uploads/sites/528/2018/12/liprofile-656x369.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                    Positioned(
                      child: CircleAvatar(
                        radius: 50,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: Image.network(
                            "https://www.youloveit.com/uploads/posts/2020-11/1605537237_youloveit_com_disney_princess_wears_masks_profile_pictures09.jpeg",
                            width: 100,
                            height: 100,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      top: 4,
                    ),
                    Positioned(
                      child: Text(
                        userName,
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                      bottom: 40,
                    ),
                    Positioned(
                      child: Text(
                        mobileNumber,
                        style: TextStyle(fontSize: 16, color: Colors.white),
                      ),
                      bottom: 15,
                    )
                  ],
                ),
              ),
              Container(
                //padding: padding,
                child: Column(
                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    buildMenuItem(
                        text: "Home",
                        icon: Icons.home_outlined,
                        onClicked: () => selectedItem(context, 0)),
                    SizedBox(height: 12),
                    buildMenuItem(
                        text: "Category",
                        icon: Icons.category_outlined,
                        onClicked: () => selectedItem(context, 1)),
                    SizedBox(height: 12),

                    buildMenuItem(
                        text: "Location",
                        icon: Icons.location_on_outlined,

                        onClicked: () => selectedItem(context, 2)),
                    SizedBox(height: 12),
                    buildMenuItem(
                      text: "More",
                      icon: Icons.widgets_outlined,
                        onClicked: () => selectedItem(context, 3)),
                    SizedBox(height: 12),
                    buildMenuItem(
                        text: "Audio",
                        icon: Icons.mic_none_outlined,
                        onClicked: () => selectedItem(context, 4)),
                    SizedBox(height: 12),
                    Divider(
                      color: Colors.blue[900],
                    ),
                    SizedBox(height: 12),
                    buildMenuItem(text: "LogOut", icon: Icons.logout),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildMenuItem({
    String text,
    IconData icon,
    VoidCallback onClicked,
  }) {
    final color = Colors.blue[900];
    final hoverColor = Colors.blue[100];

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text,
          style: TextStyle(
              color: color, fontWeight: FontWeight.bold, fontSize: 18)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    Navigator.of(context).pop();

    switch (index) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => HomeScreen(),
        ));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => CategoryScreen(),
        ));
        break;
      case 2:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => MapScreen(),

        ));
        break;
      case 3:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ExtraScreen(),
        ));
        break;
      case 4:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => AudioScreen(),
        ));
        break;
    }
  }

//
}
