import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CommonUiViews {
  static var instance = CommonUiViews();

  Widget createTextField({
    @required BuildContext context,
    TextEditingController controller,
    FocusNode focusNode,
    TextInputType textInputType,
    TextInputAction textInputAction,
    IconButton suffixIcon,
    IconButton prefixIcon,
    Color backgroundColor = Colors.black87,
    String hint = 'this is a hint message',
    TextStyle textStyle,
    String label = "This is a label message",
    Function onSubmitted,
    bool obscureText = false,
  }) {
    return Padding(
      padding: EdgeInsets.all(12.0),
      child: TextField(
        controller: controller,
        focusNode: focusNode,
        textInputAction: textInputAction,
        obscureText: obscureText,
        keyboardType: textInputType,
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Theme.of(context).primaryColor,width: 0.8)),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Theme.of(context).primaryColor,width: 0.8)),
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          hintText: hint,
          labelText: label,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: BorderSide(color: Theme.of(context).primaryColor,width: 0.8)),
           fillColor: Theme.of(context).primaryColor,
          // focusColor: Theme.of(context).primaryColor,
          // hoverColor: Theme.of(context).primaryColor,
        ),
style: textStyle,
      ),
    );
  }

  Widget creatCustomButton(
      {@required BuildContext context,
      double width = 300,
      double height = 60,
      double padding = 8,
      Icon icon,
      Function() onPressed,
      String title = "Click Me!",
      Color backgroundColor,
      double size = 0.0,
      s}) {
    return Container(
      width: width,
      height: height,
      padding: EdgeInsets.all(padding),
      child: ElevatedButton(
        onPressed: onPressed,
        child: Text(
          title,
          style: TextStyle(
              color: Colors.white, fontSize: size, fontStyle: FontStyle.italic,),
        ),
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(backgroundColor == null
                ? Theme.of(context).primaryColor
                : backgroundColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)))),
      ),
    );
  }
}
