import 'dart:io';

import 'package:first_app/utils/helper/list_sound_recorder.dart';
import 'package:first_app/utils/helper/sound_recodrer.dart';
import 'package:first_app/utils/ui/app_bar_helper.dart';
import 'package:first_app/utils/ui/common_widget_view.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class AudioScreen extends StatefulWidget {
  //final String _title;
  const AudioScreen({Key key,})  :
  super(key: key);


  @override
  _AudioScreenState createState() => _AudioScreenState();
}

class _AudioScreenState extends State<AudioScreen> {
  final recorder = SoundRecorder();
  Directory appDirectory;
  List<String> records = [];

  @override
  void initState() {
    super.initState();
    getApplicationDocumentsDirectory().then((value) {
      appDirectory = value;
      appDirectory.list().listen((onData) {
        if (onData.path.contains('.aac')) records.add(onData.path);
      }).onDone(() {
        records = records.reversed.toList();
        setState(() {});
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    appDirectory.delete();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarHelper.instance
          .createAppBar(context: context, title: "Audio Screen"),
      body: Column(
        children: [
          Expanded(
            flex: 2,
            child: RecordListView(
              records: records,
            ),
          ),
          Expanded(
            flex: 1,
            child: SoundRecorder(
              onSaved: _onRecordComplete,
            ),
          ),
        ],
      ),
    );
  }

  // Widget buildStart() {
  //   final isRecording = recorder.isRecording;
  //   final icons = isRecording ? Icons.stop : Icons.mic;
  //   final text = isRecording ? 'Stop' : 'Start';
  //   return CommonUiViews.instance.creatCustomButton(
  //       context: context,
  //       title: text,
  //       size: 20,
  //       icon: Icon(icons),
  //       onPressed: () async {
  //         //  recorder.toggleRecording();
  //         final isRecording = await recorder.toggleRecording();
  //         setState(() {});
  //       });
  // }
  _onRecordComplete() {
    records.clear();
    appDirectory.list().listen((onData) {
      if (onData.path.contains('.aac')) records.add(onData.path);
    }).onDone(() {
      records.sort();
      records = records.reversed.toList();
      setState(() {});
    });
  }
}

