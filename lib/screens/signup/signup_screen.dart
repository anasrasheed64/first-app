import 'package:first_app/screens/login/login_screen.dart';
import 'package:first_app/services/api_service.dart';
import 'package:first_app/utils/ui/app_bar_helper.dart';
import 'package:first_app/utils/ui/common_widget_view.dart';
import 'package:first_app/utils/ui/popup_helper.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  TextEditingController _nameController = TextEditingController();
  FocusNode _nameFocusNode = FocusNode();
  TextEditingController _userNameController = TextEditingController();
  FocusNode _userNameFocusNode = FocusNode();
  TextEditingController _emailController = TextEditingController();
  FocusNode _emailFocusNode = FocusNode();
  TextEditingController _mobileController = TextEditingController();
  FocusNode _mobileFocusNode = FocusNode();
  TextEditingController _passwordController = TextEditingController();
  FocusNode _passwordFocusNode = FocusNode();
  TextEditingController _confirmPasswordController = TextEditingController();
  FocusNode _confirmPasswordFocusNode = FocusNode();

  bool _passwordObsureText = true;
  bool _confirmPasswordObsureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarHelper.instance.createAppBar(
          context: context,
          title: "SignUp Screen",
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_outlined))),
      body: Center(
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CommonUiViews.instance.createTextField(
                    context: context,
                    controller: _nameController,
                    focusNode: _nameFocusNode,
                    textStyle: TextStyle(fontWeight: FontWeight.w600),
                    textInputType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    hint: " Name",
                    label: " Name",
                    prefixIcon: IconButton(
                      icon: Icon(Icons.person_outline),
                      color: Theme.of(context).primaryColor,
                      onPressed: () {},
                    ),
                    onSubmitted: () {
                      FocusScope.of(context).requestFocus(_userNameFocusNode);
                    }),
                CommonUiViews.instance.createTextField(
                    context: context,
                    controller: _userNameController,
                    focusNode: _userNameFocusNode,
                    textStyle: TextStyle(fontWeight: FontWeight.w600),
                    textInputType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    hint: "User Name",
                    label: "User Name",
                    prefixIcon: IconButton(
                      icon: Icon(Icons.person_outline),
                      color: Theme.of(context).primaryColor,
                      onPressed: () {},
                    ),
                    onSubmitted: () {
                      FocusScope.of(context).requestFocus(_emailFocusNode);
                    }),
                CommonUiViews.instance.createTextField(
                    context: context,
                    controller: _emailController,
                    focusNode: _emailFocusNode,
                    textStyle: TextStyle(fontWeight: FontWeight.w600),
                    textInputType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    hint: "Email",
                    label: "Email",
                    prefixIcon: IconButton(
                      icon: Icon(Icons.email_outlined),
                      color: Theme.of(context).primaryColor,
                      onPressed: () {},
                    ),
                    onSubmitted: () {
                      FocusScope.of(context).requestFocus(_mobileFocusNode);
                    }),
                CommonUiViews.instance.createTextField(
                    context: context,
                    controller: _mobileController,
                    focusNode: _mobileFocusNode,
                    textStyle: TextStyle(fontWeight: FontWeight.w600),
                    textInputType: TextInputType.phone,
                    textInputAction: TextInputAction.next,
                    hint: "Mobile Number",
                    label: "Mobile Number",
                    prefixIcon: IconButton(
                      icon: Icon(Icons.phone_outlined),
                      color: Theme.of(context).primaryColor,
                      onPressed: () {},
                    ),
                    onSubmitted: () {
                      FocusScope.of(context).requestFocus(_passwordFocusNode);
                    }),
                CommonUiViews.instance.createTextField(
                    context: context,
                    controller: _passwordController,
                    focusNode: _passwordFocusNode,
                    textStyle: TextStyle(fontWeight: FontWeight.w600),
                    textInputType: TextInputType.streetAddress,
                    textInputAction: TextInputAction.next,
                    hint: "Password",
                    label: "Password",
                    obscureText: _passwordObsureText,
                    suffixIcon: IconButton(
                      icon: Icon(_passwordObsureText
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_outlined),
                      color: Theme.of(context).primaryColor,
                      onPressed: () {
                        setState(() {
                          _passwordObsureText = !_passwordObsureText;
                        });
                      },
                    ),
                    onSubmitted: () {
                      FocusScope.of(context)
                          .requestFocus(_confirmPasswordFocusNode);
                    }),
                CommonUiViews.instance.createTextField(
                    context: context,
                    controller: _confirmPasswordController,
                    focusNode: _confirmPasswordFocusNode,
                    textStyle: TextStyle(fontWeight: FontWeight.w600),
                    textInputType: TextInputType.streetAddress,
                    textInputAction: TextInputAction.done,
                    hint: "Confirm Password",
                    label: "ConfirmPassword",
                    obscureText: _confirmPasswordObsureText,
                    suffixIcon: IconButton(
                      icon: Icon(_confirmPasswordObsureText
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_outlined),
                      color: Theme.of(context).primaryColor,
                      onPressed: () {
                        setState(() {
                          _confirmPasswordObsureText =
                              !_confirmPasswordObsureText;
                        });
                      },
                    ),
                    onSubmitted: () {
                      FocusScope.of(context).requestFocus();
                    }),
                SizedBox(
                  height: 12,
                ),
                CommonUiViews.instance.creatCustomButton(
                    context: context,
                    title: "Sign Up",
                    size: 20,
                    padding: 4,
                    onPressed: () async {
                      http.Response response;
                      response = await ApiService().postRequest(
                        context: context,
                        url: "https://foryo.store/api/register",
                        body: {
                          "name": _nameController.text,
                          "username": _userNameController.text,
                          "email": _emailController.text,
                          "phone": _mobileController.text,
                          "password": _passwordController.text,
                          "password_confirmation":
                              _confirmPasswordController.text,
                          "device_token": "123456"
                        },
                      );
                      if (response.statusCode == 200) { // success
                        // registrationResponseFromJson(response.body);
                        PobUpHelper.instance.showAlertDialog(
                            context: context,
                            message: 'You have Successfully registered',
                            completionHendler: () {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) => LogInScreen()),
                              );
                            });
                      } else {
                        PobUpHelper.instance.showAlertDialog(
                            context: context, message: response.body);
                      }
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool isFeildValid() {
    if (_userNameController.text == null || _userNameController.text.isEmpty) {
      _handleErrorValidation(
          msg: "User Name Is Required", focusNode: _userNameFocusNode);
      return false;
    }
    if (_mobileController.text == null || _mobileController.text.isEmpty) {
      _handleErrorValidation(
          msg: "Mobile Is Required", focusNode: _mobileFocusNode);
      return false;
    }
    if (_passwordController.text == null || _passwordController.text.isEmpty) {
      _handleErrorValidation(
          msg: "Password Is Required", focusNode: _passwordFocusNode);
      return false;
    }
    if (_confirmPasswordController.text == null ||
        _confirmPasswordController.text.isEmpty) {
      _handleErrorValidation(
          msg: "Confirm Password Is Required",
          focusNode: _confirmPasswordFocusNode);
      return false;
    }
    if (_confirmPasswordController.text != _passwordController) {
      _handleErrorValidation(
          msg: "Password and Confirm Password not Matched",
          focusNode: _passwordFocusNode);
      return false;
    }
    return true;
  }

  void _handleErrorValidation({String msg, FocusNode focusNode}) {
    PobUpHelper.instance.showAlertDialog(
        context: context,
        button: 'OK',
        message: msg,
        completionHendler: () {
          FocusScope.of(context).requestFocus(focusNode);
        });
  }
}
