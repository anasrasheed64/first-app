import 'package:cached_network_image/cached_network_image.dart';
import 'package:first_app/models/category.dart';
import 'package:first_app/models/product.dart';
import 'package:first_app/screens/category_screen/category_screen.dart';
import 'package:first_app/screens/details/datails_screen.dart';
import 'package:first_app/utils/helper/database_helper.dart';
import 'package:first_app/utils/helper/demo_data.dart';
import 'package:first_app/utils/ui/common_widget_view.dart';
import 'package:flutter/material.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({Key key}) : super(key: key);

  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  // List<Category> category = [
  //   Category(name: "fruit", imagePath: 'assets/fruit.png'),
  //   Category(name: "vegetabels", imagePath: 'assets/vegetabel.png'),
  //   Category(name: 'bakery', imagePath: 'assets/bread.png'),
  //   Category(name: 'dairy', imagePath: 'assets/dairy.png'),
  // ];
  // List<Product> product = [
  //   Product(
  //       name: "Strowberry",
  //       price: 2.5,
  //       img: "assets/strowberry.png",
  //       desc: "red fruit",quantity: 0),
  //   Product(
  //       name: "Bnana", price: 2.5, img: "assets/bnana.png", desc: "red fruit",quantity: 0),
  //   Product(
  //       name: "Kiwi", price: 2.5, img: "assets/kiwi.png", desc: "red fruit",quantity: 0),
  //   Product(
  //       name: "Watermelon",
  //       price: 2.5,
  //       img: "assets/watermelon.png",
  //       desc: "red fruit",quantity: 0),
  // ];
  List<Category> category;

  //late List<Product>product;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    category = DemoData.categories;
    // product=DemoData.products;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: MediaQuery.of(context).size.width,
      // height: MediaQuery.of(context).size.height*0.4,
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(5),
            child: Image.asset(
              'assets/main.jpg',
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.28,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Categories",
                  style: TextStyle(
                    fontSize: 16,
                    // fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600,
                    //fontFamily: 'Lobster-Regular'
                  ),
                ),
                CommonUiViews.instance.creatCustomButton(
                    context: context,
                    title: "Explore All",
                    size: 13,
                    width: MediaQuery.of(context).size.width * 0.25,
                    height: MediaQuery.of(context).size.height * 0.06,
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => CategoryScreen()));
                    }),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            // color: Colors.blue,
            child: Center(
              child: ListView.builder(
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    padding: EdgeInsets.all(12),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          category[index].imagePath.toString(),
                          width: 90,
                          height: 80,
                        ),
                        Text(
                          category[index].name.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 13),
                        ),
                      ],
                    ),
                  );
                },
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemCount: category.length,
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.01,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Top Products",
                  style: TextStyle(
                    fontSize: 16,
                    // fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600,
                    //fontFamily: 'Lobster-Regular'
                  ),
                ),
                CommonUiViews.instance.creatCustomButton(
                    context: context,
                    title: "Explore All",
                    size: 13,
                    width: MediaQuery.of(context).size.width * 0.25,
                    height: MediaQuery.of(context).size.height * 0.06,
                    onPressed: () {}),
              ],
            ),
          ),
          Container(
            height: 120,
            child: Center(
                child: FutureBuilder<List<Product>>(
              future: _getProductsFromSqliteDataBase(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Product>> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return Text("Loading!...");
                  default:
                    List<Product> products = snapshot.data;
                    if (snapshot.hasError)
                      return Text("Erorr:${snapshot.error}");
                    else {
                      List<Product> products = snapshot.data;
                      return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          physics: const AlwaysScrollableScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => DetalisScreen(
                                          product: products[index],
                                          relatedProducts: products,
                                        )));
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      products[index].img.toString(),
                                      width: 120,
                                      height: 90,
                                    ),
                                    Text(
                                      products[index].name.toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 13),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                          itemCount: products.length);
                    }
                }
              },
            )),
          ),
        ],
      ),
      //   child: ListView.builder(itemBuilder: ),
    );
  }

  Future<List<Product>> _getProductsFromSqliteDataBase() async {
    var instance = DatabaseHelper();
    return await instance.getAllProducts();
  }
}
