//import 'package:cached_network_image/cached_network_image.dart';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import "package:image_picker/image_picker.dart";

class ProfileTab extends StatefulWidget {
  const ProfileTab({Key key}) : super(key: key);

  @override
  _ProfileTabState createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  File _image;
  final imagePicker=ImagePicker();
  Future getImage()async{
    final image=await imagePicker.getImage(source: ImageSource.camera);
    setState(() {
      _image=File(image.path);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 50,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: _image==null?Text("No Image"):Image.file(_image,fit: BoxFit.fill,),
                // CachedNetworkImage(
                //   imageUrl:
                //       "https://www.youloveit.com/uploads/posts/2020-11/1605537237_youloveit_com_disney_princess_wears_masks_profile_pictures09.jpeg",
                //   width: 400,
                //   height: 400,
                //   fit: BoxFit.fill,
                // ),
              ),
            ),
            Text(
              "User Name",
              style: TextStyle(
                  fontSize: 20, color: Theme.of(context).primaryColor),
            ),
            Text(
              "Mobile Number",
              style: TextStyle(
                  fontSize: 20, color: Theme.of(context).primaryColor),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(Icons.camera_alt_outlined),
        onPressed: getImage,
      ),
    );
  }
}
