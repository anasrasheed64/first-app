import 'package:first_app/screens/home/tabs/home_tab.dart';
import 'package:first_app/screens/home/tabs/profile_tab.dart';
import 'package:first_app/screens/login/login_screen.dart';
import 'package:first_app/utils/ui/navigation_drawer.dart';
import 'package:first_app/utils/ui/popup_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class HomeScreen extends StatefulWidget {
  final int index;

  const HomeScreen({Key key, this.index = 0}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // List<Category> category = [
  //   Category(name: "fruit", imagePath: 'assets/fruit.png'),
  //   Category(name: "vegetabels", imagePath: 'assets/vegetabel.png'),
  //   Category(name: 'bakery', imagePath: 'assets/bread.png'),
  //   Category(name: 'dairy', imagePath: 'assets/dairy.png'),
  // ];
  List<Widget> tabs = [
    HomeTab(),
    ProfileTab(),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: widget.index,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Home Screen"),
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: () {
                  PobUpHelper.instance.showPobUpWithButtons(
                      context: context,
                      title: "Warning",
                      type: AlertType.warning,
                      desc: "Are You Sure You Want To Logout?",
                      textStyle: TextStyle(fontWeight: FontWeight.w600),
                      positiveLabel: "Sure",
                      negativeLabel: "Cancel",
                      positinePressed: () {
                        setState(() {});
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => LogInScreen()),
                            (route) => false);
                      });
                },
                icon: Icon(Icons.power_settings_new_outlined))
          ],
          bottom: TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.home_outlined),
              ),
              Tab(
                icon: Icon(Icons.person),
              ),
            ],
          ),
        ),

        drawer: NavigationDrawer(),
        //Drawer(child: Material(
        //   child: Container(
        //     child: ListView(
        //       padding: EdgeInsets.symmetric(horizontal: 20),
        //       children: [
        //         SizedBox(
        //           height: 48,
        //         ),
        //        Text("Home"),
        //         Icon(Icons.home_outlined),
        //       ],
        //     ),
        //   ),
        // ),),
        body: TabBarView(children: tabs),
      ),
    );
  }
}
