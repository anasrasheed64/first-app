import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/flutter_map.dart' as marker;
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
//import 'package:google_maps_flutter/google_maps_flutter.dart' as marker;
import 'package:latlong/latlong.dart' as latLng;
// import 'package:latlong/';

class MapScreen extends StatefulWidget {
  const MapScreen({Key key}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng point = LatLng(49.5, -0.09);
  double long = 49.5;
  double lat = -0.09;

  var location = [];

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        FlutterMap(
          options: MapOptions(
              onTap: (p) async {
                location = await Geocoder.local.findAddressesFromCoordinates(
                    new Coordinates(p.latitude, p.longitude));
                setState(() {
                  point = LatLng(p.latitude, p.longitude);
                  print(p);
                });
                print(
                    "${location.first.countryName} - ${location.first.featureName}");
              },
              center: latLng.LatLng(49.5, -0.09),
              zoom: 10.0),
          layers: [
            TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c'],
            ),
            MarkerLayerOptions(
              markers: [
                marker.Marker(
                  width: 80.0,
                  height: 80.0,
                  builder: (ctx) => Icon(
                    Icons.location_on,
                    color: Colors.red,
                    size: 40.0,
                  ),
                  point: latLng.LatLng(point.latitude, point.longitude),
                ),
              ],
            ),
          ],
        ),
        // Padding(
        //     padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 34.0),
        //     child: Column(
        //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //       children: [
        //         Card(
        //           child: Padding(
        //             padding: const EdgeInsets.all(8.0),
        //             child: Column(
        //               children: [
        //                 Text(
        //                     "${location.first.countryName},${location.first.locality}, ${location.first.featureName}"),
        //               ],
        //             ),
        //           ),
        //         ),
        //       ],
        //     )),
      ],
    );
  }
}
