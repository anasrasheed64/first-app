import 'package:first_app/models/category.dart';
import 'package:first_app/utils/ui/search_widget.dart';
import 'package:first_app/utils/ui/app_bar_helper.dart';
import 'package:first_app/utils/ui/navigation_drawer.dart';
import 'package:flutter/material.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({Key key}) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
//  String searchQuery = "Search query";
  List<Category> categories = [
    Category(name: "fruit", imagePath: 'assets/fruit.png'),
    Category(name: "vegetabels", imagePath: 'assets/vegetabel.png'),
    Category(name: 'bakery', imagePath: 'assets/bread.png'),
    Category(name: 'dairy', imagePath: 'assets/dairy.png'),
    // Category(name: 'bakery', imagePath: 'assets/bread.png'),
    // Category(name: 'dairy', imagePath: 'assets/dairy.png'),
  ];

  String query = '';

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarHelper.instance
          .createAppBar(context: context, title: "Category Screen", actions: [
        IconButton(
            onPressed: () {
              showSearch(
                  context: context, delegate: CategoryItemSearch(categories));
            },
            icon: Icon(Icons.search_outlined)),
      ]),
      drawer: NavigationDrawer(),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              //buildSearch(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.1,
              ),
              Column(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.7,
                    width: MediaQuery.of(context).size.width * 0.8,
                    //padding: EdgeInsets.all(20),

                    child: GridView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: categories.length,
                        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: 200,
                          //childAspectRatio: 3/2,
                          crossAxisSpacing: 15,
                          mainAxisSpacing: 20,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            // width: 2,
                            // height: 1,
                            padding: EdgeInsets.all(12),
                            child: Card(
                              color: Theme.of(context).cardColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    categories[index].imagePath.toString(),
                                    width: 110,
                                    height: 100,
                                  ),
                                  Text(
                                    categories[index].name.toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

// Widget buildSearch() => SearchWidget(
//   text: query,
//   hintText: 'Category Name',
//   onChanged: searchCategory,
//
// );
// Widget buildCategory(Category category) => ListTile(
//   leading: Image.network(
//    category.imagePath,
//     fit: BoxFit.cover,
//     width: 50,
//     height: 50,
//   ),
//   title: Text(category.name),
//
// );
//
// void searchCategory(String query) {
//   final categories = category.where((category) {
//     final categoryName= category.name!.toLowerCase();
//     //final authorLower = category.author.toLowerCase();
//     final searchName = query.toLowerCase();
//
//     return categoryName!.contains(searchName) ;
//   }).toList();
//
//   setState(() {
//     this.query = query;
//     //this.books = books;
//     this.category=category;
//   });
// }
}
