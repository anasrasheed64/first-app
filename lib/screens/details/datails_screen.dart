import 'package:first_app/models/product.dart';
import 'package:first_app/screens/details/views/card_view.dart';
import 'package:first_app/screens/details/views/shop_view.dart';
import 'package:first_app/utils/ui/common_widget_view.dart';
import 'package:flutter/material.dart';

class DetalisScreen extends StatefulWidget {
  //const DetalisScreen({Key? key}) : super(key: key);
  final Product product;
  final List<Product> relatedProducts;

  //final int index;

  // DetalisScreen(this.product, this.relatedProducts, this.index);

  DetalisScreen({this.product,  this.relatedProducts});

  @override
  _DetalisScreenState createState() => _DetalisScreenState();
// _DetalisScreenState createState() => _DetalisScreenState(product,relatedproduct);
}

class _DetalisScreenState extends State<DetalisScreen> {
  //final PageController controller = PageController(initialPage: 0);

  // final Product product;
  // final List<Product> relatedproduct;
  //
  // _DetalisScreenState(this.product, this.relatedproduct);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: NestedScrollView(
          physics: NeverScrollableScrollPhysics(),
          floatHeaderSlivers: false,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverOverlapAbsorber(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                sliver: SliverAppBar(
                  expandedHeight: MediaQuery.of(context).size.height / 3,
                  backgroundColor: Colors.white,
                  pinned: false,
                  flexibleSpace: FlexibleSpaceBar(
                    background: Image.asset(
                      widget.product.img,
                      fit: BoxFit.cover,
                    ),
                  ),
                  leading: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Icon(
                      Icons.arrow_back_outlined,
                      color: Colors.black,
                    ),
                  ),
                  bottom: TabBar(tabs: [
                    Tab(
                        child: Icon(
                      Icons.shopping_basket_outlined,
                      color: Colors.blue[900],
                    )),
                    Tab(
                        child: Icon(Icons.category_outlined,
                            color: Colors.blue[900])),
                  ]),
                ),
              ),
            ];
          },
          body: PageView(children: [
            TabBarView(children: [
              ShopView(widget.product, widget.relatedProducts),
              CardView(),
            ])
          ]),
        ),
      ),
    );
  }
}
