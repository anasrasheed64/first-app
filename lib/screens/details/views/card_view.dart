import 'package:first_app/models/product.dart';
import 'package:first_app/screens/home/home_screen.dart';
import 'package:first_app/utils/helper/current_session.dart';
import 'package:first_app/utils/ui/common_widget_view.dart';
import 'package:first_app/utils/ui/popup_helper.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class CardView extends StatefulWidget {
  const CardView({Key key}) : super(key: key);

  @override
  _CardViewState createState() => _CardViewState();
}

class _CardViewState extends State<CardView> {
  List<Product> orderProducts;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    orderProducts = CurrentSession.instance.cartProducts;
  }

  @override
  Widget build(BuildContext context) {
    if (orderProducts == null || orderProducts.isEmpty)
      return Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Your Cart Is Empty",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).primaryColor),
              ),
              CommonUiViews.instance.creatCustomButton(
                  context: context,
                  title: "Show Products",
                  size: 20,
                  width: MediaQuery.of(context).size.width * 0.4,
                  height: MediaQuery.of(context).size.height * 0.075,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeScreen()));
                  }),
            ],
          ),
        ),
      );
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              width: MediaQuery.of(context).size.width * 0.5,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(12),
                          alignment: Alignment.centerLeft,
                          child: Image.asset(
                            orderProducts[index].img,
                            width: 100,
                            height: 100,
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Text(
                                orderProducts[index].name,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '\$${orderProducts[index].price}',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 50),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4),
                            ),
                            color: Colors.grey,
                            child: Row(
                              children: [
                                Container(
                                  height: 30,
                                  width: 30,
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.remove,
                                      size: 16,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        if (orderProducts[index].quantity > 1)
                                          orderProducts[index].quantity--;
                                        else
                                          PobUpHelper.instance
                                              .showPobUpWithButtons(
                                                  context: context,
                                                  type: AlertType.warning,
                                                  desc:
                                                      "Are You Sure You Want To Remove This Item?",
                                                  title: "Warning",
                                                  negativeLabel: "Cancel",
                                                  positiveLabel: "Sure",
                                                  positinePressed: () {
                                                    setState(() {
                                                      orderProducts.remove(
                                                          orderProducts[index]);
                                                    });
                                                  });
                                      });
                                    },
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    orderProducts[index].quantity.toString(),
                                  ),
                                  color: Colors.white,
                                  height: 30,
                                  width: 30,
                                ),
                                Container(
                                  height: 30,
                                  width: 30,
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.add,
                                      size: 16,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        orderProducts[index].quantity++;
                                      });
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: orderProducts.length,
        ),
      ),
    );

  }
  String _getTotalAmount(){
    double amount=0;
    for(var product in orderProducts){
      amount+=product.price*product.quantity;
    }
    return amount.toString();
  }
}
