import 'package:first_app/models/product.dart';
import 'package:first_app/screens/details/datails_screen.dart';
import 'package:first_app/utils/helper/current_session.dart';
import 'package:first_app/utils/ui/common_widget_view.dart';
import 'package:first_app/utils/ui/popup_helper.dart';
import 'package:flutter/material.dart';

class ShopView extends StatefulWidget {
  List<Product> relatedProducts;
  final Product product;

  ShopView(this.product, this.relatedProducts);

  @override
  _ShopViewState createState() => _ShopViewState();
}

class _ShopViewState extends State<ShopView> {
  int count = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: CommonUiViews.instance.creatCustomButton(
          context: context,
          title: "Add To Cart",
          size: 20,
          icon: Icon(Icons.shopping_basket_outlined),
          padding: 8,
          onPressed: () {
            if (count == 0) return;
            _addToCartList();
            print("clicked!");
            PobUpHelper.instance.showSuccessSnackBar(
                context, "${widget.product.name},added to your Cart");
          }),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: Container(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.product.name,
                    style: TextStyle(
                        color: Theme.of(context).primaryColor, fontSize: 20),
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                    color: Colors.grey,
                    child: Row(
                      children: [
                        Container(
                          height: 30,
                          width: 30,
                          child: IconButton(
                            icon: Icon(
                              Icons.remove,
                              size: 16,
                            ),
                            onPressed: () {
                              setState(() {
                                if (count != 0) count--;
                              });
                            },
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: Text(count.toString()),
                          color: Colors.white,
                          height: 30,
                          width: 30,
                        ),
                        Container(
                          height: 30,
                          width: 30,
                          child: IconButton(
                            icon: Icon(
                              Icons.add,
                              size: 16,
                            ),
                            onPressed: () {
                              setState(() {
                                count++;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "\$${widget.product.price}",
                    style: TextStyle(
                        color: Theme.of(context).primaryColor, fontSize: 20),
                  ),
                  Text(
                    "Kg",
                    style: TextStyle(
                        color: Theme.of(context).primaryColor, fontSize: 15),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                widget.product.desc,
                style: TextStyle(fontSize: 16),
                maxLines: 6,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(12),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Related Product",
                  style: TextStyle(fontSize: 24),
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.18,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  physics: const AlwaysScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => DetalisScreen(
                                  product: widget.relatedProducts[index],
                                  relatedProducts: widget.relatedProducts,
                                )));
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.57,
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(8),
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                      widget.relatedProducts[index].img
                                          .toString(),
                                      width: 80,
                                      height: 70,
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                        widget.relatedProducts[index].name
                                            .toString(),
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        "\$${widget.relatedProducts[index].price.toString()}",
                                        style: TextStyle(fontSize: 20),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: widget.relatedProducts.length),
            ),
          ],
        ),
      ),
    );
  }

  void _addToCartList() {
    bool isExist = false;
    List<Product> cartList = CurrentSession.instance.cartProducts;
    for (var item in cartList) {
      if (item.name == widget.product.name) {
        item.quantity += count;
        isExist = true;
        break;
      }
    }
    if (!isExist) {
      widget.product.quantity = count;
      cartList.add(widget.product);
    }
    setState(() {
      CurrentSession.instance.cartProducts = cartList;
    });
  }
}
